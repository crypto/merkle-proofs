# Vérification d'identité sans révéler d'information

## Objectif

L'objectif de ce projet est de mettre en place un système permettant de prouver et de vérifier son identité sans révéler d'information permettant d'être identifié.
Pour cela, il vous faudra écrire des circuits qui permettent de prouver et verifier son appartenance à un groupe à l'aide d'une structure de donnée appelée arbre de Merkle.

## Introduction

Prouver son identité est une opération courante dans la vie de tous les jours, pour, par exemple, accéder à un service sur l'Internet. 
Mais il peut y avoir des cas où l'on souhaite prouver son identité sans révéler d'information. Par exemple, prouver que l'on est majeur sans révéler sa date de naissance.
Pour cela, les preuves de Merkle sont une solution. L'utilisateur prouve qu'il fait partie d'un groupe sans révéler son identité.

### Les preuves de Merkle

Les preuves de Merkle sont des preuves cryptographiques qui permettent de prouver qu'un élément fait partie d'un ensemble sans révéler l'ensemble.
Un arbre de Merkle est un arbre binaire dans lequel chaque nœud interne est le hash des nœuds fils. La racine de l'arbre est appelée racine de Merkle.
Pour prouver qu'un élément fait partie de l'arbre, il suffit de fournir la liste des hashs qui permettent de remonter de l'élément à la racine de l'arbre.
La vérification de la preuve consiste à recomposer le chemin de l'élément jusqu'à la racine de l'arbre, et à vérifier que les hashs correspondent.

### Le systeme

Supposons un système dans lequel l'état gère une liste d'utilisateurs appartenant à un groupe, par exemple, le groupe de tous les citoyens.

Il génère d'abord un arbre de Merkle avec tous les utilisateurs et fournit un document d'identité à chaque utilisateur, correspondant à une feuille de l'arbre. Ce document est associé à une preuve de Merkle.
Cette preuve permet de prouver son appartenance au groupe sans devoir dévoiler le document d'identité.

Par ailleurs, l'état fournit de manière publique la racine de l'arbre de Merkle pour que tout le monde puisse vérifier l'appartenance d'un utilisateur au groupe.

Lorsqu'un citoyen souhaite accéder à un site Internet, il doit prouver qu'il est bien un citoyen et qu'il est majeur au moment de l'accès.
Pour ce faire, le site récupère la racine de l'arbre de Merkle. L'utilisateur génère une preuve ZK prouvant qu'il est bien dans l'arbre, ainsi que sa majorité.

```mermaid

flowchart BT
A("`ID 1
Nom : **Toto**
Prenom : **Tata**`") --> H1["`H1 = hash(ID1)`"]
B("`ID 2
Nom : **Toto**
Prenom : **Tata**`") --> H2["`H2 = hash(ID2)`"]
C("`ID 3
Nom : **Toto**
Prenom : **Tata**`") --> H3["`H3 = hash(ID3)`"]
D("`ID 4
Nom : **Toto**
Prenom : **Tata**`") --> H4["`H4 = hash(ID4)`"]
H1 --> H12["`H12 = hash(H1+H2)`"]
H2 --> H12
H3 --> H34["`H34 = hash(H3+H4)`"]
H4 --> H34
H12 --> Root["`**Root**:
hash(H12+H34)`"]
H34 --> Root
```

Cette preuve, appelée Preuve de Merkle, permet, à l'aide de la racine de vérifier qu'une feuille appartient bien à l'arbre, sans connaitre l'entièreté des données.
Par exemple, le citoyen ID3, possèdera la preuve de merkle suivante : H4, H12, Root, ainsi que les sélecteurs associés [0,1]. 0 signifie que la concaténation des hashs se fait en plaçant son hash à gauche, 1 signifie qu'il faut placer son hash à droite.

## Prouver son identité

Pour prouver son identité sans révéler d'information, nous allons utiliser des circuits qui permettent de prouver que l'on possède une preuve de Merkle valide sans révéler les informations d'identité.
Le circuit prend en entrée les informations d'identité qui seront hashées, et une preuve de Merkle, et vérifie que les informations d'identité font bien partie de l'arbre de Merkle en reconstruisant la racine de l'arbre.

Bien entendu, le circuit ne doit pas révéler les informations d'identité. Les informations d'identité seront privées, et la preuve de Merkle, consitutée des hashs et des sélecteurs, sera quant à elle publique.

Avant d'écrire un circuit faisant tout cela, nous allons voir que le zero-knowledge implique l'utilisation d'algorithmes légèrement différents de ceux que nous avons vu jusqu'à présent.

Certains de ces algorithmes sont implémentés dans une librairie qui accompagne circom, appelée `circomlib`.

## Exercices avancés avec circomlib

Circomlib est une bibliothèque de composants Circom. Elle propose différents composants paramétrables, ou `template`, prêts à l'emploi.

Nous allons utiliser certains composants de `circomlib` pour écrire plus facilement des circuits plus complexes.


### 1. Comparaisons de signaux en circom

Ecrire un circuit `IsOver18.circom`. Ce circuit instantie un template IsOver18 qui prend en entrée un signal correspondant à l'age d'une personne et retourne en sortie un signal binaire indiquant si la personne est majeure ou non.

Naturellement, il vient à l'esprit de faire une comparaison entre l'age et 18 avec un `if`. Essayez de compiler un circuit avec ce composant :

```text
template IsOver18() {
    signal input age;
    signal output oldEnough;
    
    if (age >= 18) {
        oldEnough <== 1;
    } else {
        oldEnough <== 0;
    }
}
```

Vous devriez voir une erreur de compilation :  les circuits ne peuvent pas contenir un nombre de contraintes variable, dépendant des valeurs des signaux.
Nous allons donc utiliser un composant de circomlib pour comparer les signaux d'une autre manière.

Un autre problème est que les operations d'addition, de soustraction, de multiplications sur les signaux sont effectuées sur un corps finis. Ainsi, exécuter une soustraction entre deux signaux peut donner un résultat complètement différent de ce à quoi on pourrait s'attendre.
Par exemple, sur $F_p$, si `a = 1` et `b = 2`, alors `a - b = 1 - 2 = 21888242871839275222246405745257275088548364400416034343698204186575808495616`.


Pour comparer des signaux, il nous faut convertir les élements du corps fini en binaire, et comparer les bits un par un.

En conclusion, pour réaliser notre circuit, nous allons utiliser les composants `Num2Bits ` et `LessThan` de circomlib.

#### Num2Bits

Le composant `Num2Bits` prend en entrée un signal et retourne en sortie un tableau de bits correspondant à la représentation binaire de ce signal.
Il se trouve dans ce fichier : https://github.com/iden3/circomlib/blob/master/circuits/bitify.circom

```text
template Num2Bits(n) {
    signal input in;
    signal output out[n];
    var lc1=0;

    var e2=1;
    for (var i = 0; i<n; i++) {
        out[i] <-- (in >> i) & 1;
        out[i] * (out[i] -1 ) === 0;
        lc1 += out[i] * e2;
        e2 = e2+e2;
    }

    lc1 === in;
}
```

Le principe est le suivant :
- on prend chaque bit du signal d'entrée, en commençant par le bit de poids faible,  et on vérifie que la valeur est bien binaires (0 ou 1).
- on place chaque bit dans un tableau de bits `out`.
- On multiplie ensuite ce bit par une puissance de 2 et on accumule ainsi chaque résultat de la multiplication dans un signal `lc1`.
- On verifie ensuite par une contrainte que `lc1` est bien égal au signal d'entrée.

Cela revient donc à recalculer le signal d'entrée à partir de ses bits. En sortie, on obtient bien un tableau de bits qui représente le signal d'entrée.

Notez que ce composant est paramétré par la taille du tableau de bits `n`. Plus n est grand, plus le nombre pourra être grand, mais plus le circuit sera complexe.


#### LessThan

Le composant `LessThan` prend en entrée deux tableaux de bits et retourne en sortie un signal binaire indiquant si le premier signal est inférieur au second.
Il se trouve dans ce fichier : https://github.com/iden3/circomlib/blob/master/circuits/comparators.circom

```text
template LessThan(n) {
    assert(n <= 252);
    signal input in[2];
    signal output out;

    component n2b = Num2Bits(n+1);

    n2b.in <== in[0]+ (1<<n) - in[1];

    out <== 1-n2b.out[n];
}
```

Le principe est le suivant :
- On instancie le composant `Num2Bits` avec `n+1` bits. Cela permet de comparer des nombres de `n` bits.
- On calcule la différence entre les deux nombres en ajoutant 1 << n. Cela permet de s'assurer que la différence est toujours positive (les nombres sont toujours inférieurs à 2^n).
- On vérifie ensuite si le bit de poids fort de la différence est à 0. Si c'est le cas, alors le premier nombre est inférieur au second, out vaudra donc 1.

Notez que ce composant est paramétré par la taille des tableaux de bits `n`. Plus n est grand, plus les nombres pourront être grands, mais plus le circuit sera complexe.

Ce paramètre doit être inférieur ou égal à 252. pourquoi ?

#### Exercice

- Ecrivez un circuit `IsOverN.circom` qui utilise le composant `LessThan` pour comparer un premier signal d'entrée `age` à un second signal `n` et retourne un signal binaire indiquant si `age` est supérieur ou égal à `n`.
- paramétrez correctement le composant `LessThan`en définissant la taille des tableaux de bits `n` en fonction de la taille de `age` et `n`. Choisissez une taille suffisante pour que le circuit puisse comparer des nombres jusqu'à 250.
- Compilez le circuit et testez le en générant une preuve et en la vérifiant. Testez avec un age de 18 et 17 et n = 18.

### 2. Les hashs en Circom

Comme vous l'avez vu, lorsque l'on souhaite hasher de la donnée, on utilise généralement une fonction de hashage comme SHA256.

Ecrivez le circuit hash.circom tel que :

```text
pragma circom 2.0.0;
include "../node_modules/circomlib/circuits/sha256/sha256.circom";

component main = Sha256(256);
```

Ce circuit utilise le composant `Sha256` de circomlib pour hasher un signal d'entrée de 256 bits.

Compilez ce circuit avec circom et observez le nombre de contraintes que vous obtenez.

Dans un programme classique, utiliser SHA256 est très rapide : il s'agit d'opérations mathématiques simples sur des bits comme des décalages, des rotations, des XOR, des AND, etc. Toutes ces operations sont natives pour un processeur.
Dans un circuit, ces opérations sont beaucoup plus complexes : chaque opération doit être transformée en contrainte, et chaque contrainte doit être vérifiée.

C'est pourquoi, de nouvelles fonctions de hashage ont été développées pour les circuits, comme Poseidon, Rescue, .., etc. Ces fonctions de hashage sont conçues pour être plus efficaces en terme de contraintes.

Comparez maintenant le nombre de contraintes en utilisant le composant `Poseidon` de circomlib :

```text
pragma circom 2.0.0;
include "../node_modules/circomlib/circuits/poseidon.circom";

template PoseidonHasher() {
    signal input in;
    signal output out;

    component poseidon = Poseidon(1);

    poseidon.inputs[0] <== in;

    out <== poseidon.out;
}

component main = PoseidonHasher();
```


### 3. Les sélecteurs en Circom

Supposons le cas suivant : vous devez écrire un circuit prennant en entrée deux signaux `a` et `b`, et retournant en sortie le hash de `a` et `b` ou `b` et `a` en fonction d'un signal binaire `selector`.

Une première idée serait d'utiliser un `if` pour choisir l'ordre des signaux à hasher. Cependant, les circuits ne peuvent pas contenir de `if` ou de boucles sur les signaux.

Pour résoudre ce problème, nous devons trouver un moyen d'inverser les signaux `a` et `b` en fonction de `selector`. Le tout, en respectant les règles des contraintes de circom.

Cela revient à écrire un composant qui prend en entrée deux signaux `a` et `b`, un signal binaire `selector`, et retourne en sortie `a` et `b` ou `b` et `a` en fonction de `selector`.'

Ecrivez un circuit `switcher.circom` tel que :

```text
pragma circom 2.0.0;

template Switcher() {
    signal input in[2];
    signal input selector;
    signal output out[2];

    // check if selector is 0 or 1
    
    // if selector is 0, out[0] = in[0] and out[1] = in[1]
    // if selector is 1, out[0] = in[1] and out[1] = in[0]
    
}
```

La première étape consiste à vérifier si le signal `selector` est bien binaire. Trouvez la construction qui permet de vérifier cela.

Ensuite, en fonction de s, calculer les signaux de sortie `out[0]` et `out[1]` en fonction de `in[0]` et `in[1]`. Trouvez la construction qui permet de réaliser cela.

Pour rappel, toute contrainte doit contenir au moins une multiplication.

## Les preuves de Merkle en pratique

Dans le dossier `bin`, vous trouverez le fichier `merkle_intro.ts`.

Vous pouvez exécuter ce script avec la commande suivante :

```bash
npm run merkle_intro
```

Analysez le code pour comprendre comment fonctionne l'API, and comment générer une preuve de Merkle.

### Composants Circom de base

#### `MerkleVerifier`

- Dans le dossier `circuits/components`, créez le fichier `merkle_verifier.circom`.
- Ecrivez le template `MerkleVerifier` qui prend en entrée un signal correspondant au hash des données d'identité, un signal représentant le hash racine de l'arbre, un tableau de `levels` signaux de hashs, un tableau de `levels` signaux de sélecteurs, sans sortie.
- Ce composant sera paramétré par le nombre de niveaux de l'arbre `levels`.
- Ce composant devra vérifier que le hash des données d'identité est bien dans l'arbre de Merkle en utilisant les hashs et les sélecteurs fournis.

Dans le dossier `circuits`, écrivez le fichier `merkle_verifier_2.circom` et instanciez le template `MerkleVerifier` avec `levels=2` comme étant le composant `main`.
Vous pouvez mettre en signaux public le signal `leaf`, `root`, `hashes` et `selectors`: 

```text
pragma circom 2.0.0;

include "components/merkle_verifier.circom";

component main{public [leaf,root, hashes, selectors]} = MerkleVerifier(2);
```

Verifiez la validation de votre circuit en executant les tests associés : `npm run test:merkle_verifier`.

#### `IdCheck`

- Dans le dossier `circuits/components`, créez le fichier `id_check.circom`.
- Ecrivez le template `IdCheck` qui prend en entrée les signaux suivants dans cet ordre :
    - `name`: le nom du citoyen
    - `firstname`: le prénom du citoyen
    - `birthYear`: l'année de naissance du citoyen
    - `root`: le hash racine de l'arbre de Merkle
    - `hashes`: un tableau de `levels` signaux de hashs
    - `selectors`: un tableau de `levels` signaux de sélecteurs
    - `currentYear`: l'année courante
- Ce composant est paramétré par le nombre de niveaux de l'arbre `levels`.
- Ce composant devra générer le hash des données d'identité, et vérifier que les données d'identité sont bien dans l'arbre de Merkle.
- Prouvez également que le citoyen a plus de 18 ans en fonction de l'année courrante donnée.

### Instances du circuit

Dans le dossier `circuits`, écrivez le fichier `id_check_2.circom` et instanciez le template `IdCheck` avec `levels=2` comme étant le composant `main`.
Ce circuit aura en signaux publics `root`, `hashes`, `selectors` et `currentYear`. Les signaux `name`, `firstname` et `birthYear` seront privés.

Verifiez la validation de votre circuit en executant les tests associés : `npm run test:id_check`.

# Prouver son identité sans révéler d'information

Nous allons maintenant utiliser les circuits que nous avons créés pour écrire le code du système servant à prouver son identité sans révéler d'information.

- Dans le dossier `bin`, créez le fichier `id_check.ts`.
- Générez un arbre de Merkle comprenant 4 ou 8 Citoyens et calculez la racine de l'arbre.
    - L'un des citoyens doit être mineur.
- Ecrivez le code qui permet de générer une preuve de Merkle pour chaque citoyen.
- Utilisez les classes `IdCheckProver` et `IdCheckVerifier` pour prouver et vérifier l'identité d'un citoyen.
- Essayez de générer une preuve pour le citoyen mineur et observez le résultat.

