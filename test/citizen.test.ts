import assert from "assert";
import {Citizen} from "../src/citizen";

describe("Test Citizen", function () {
    it("Should get the correct hash", async function() {
        const cit = new Citizen("Doe", "John", 1999);
        const h = await cit.hash();
        assert(h.toString() === '7860554319195248765781717048594715132505646606823508199848309929105051540681');
    });
});