import {SnarkProof} from "../src/proof";
import assert from "assert";
import {IdCheckProver} from "../src/id_check.prover";
import {MerkleTree} from "../src/merkle_tree";
import {Citizen} from "../src/citizen";
import {IdCheckVerifier} from "../src/id_check.verifier";

describe("Test IDCheck", function () {
   let prover: IdCheckProver;
   let proof: SnarkProof;
   let tree: MerkleTree<Citizen>
   beforeEach(async()  => {
       prover = new IdCheckProver(2);
       const cit1 = new Citizen("Doe", "John", 2010);
       const cit2 = new Citizen("Doe2", "John2", 1999);
       const cit3 = new Citizen("Doe3", "John3", 1999);
       const cit4 = new Citizen("Doe4", "John4", 1999);
       let citizens = Array.from([cit1,cit2,cit3,cit4]);
       tree = new MerkleTree(citizens);
   });
   it("Should generate the proof the identity of a citien", async function() {
       let cit4: Citizen = tree.get_leaf_from_id(3); // get cit4
       let merkle_proof = await tree.generate_proof(cit4);
       proof = await prover.prove(cit4,merkle_proof);
   });
   it("Should verify that a citizen has more than 18", async function() {
       let verifier = new IdCheckVerifier(2);
       let v = await verifier.verify(proof);
       assert(v);
   });
   it("Should not build a proof correctly if a citizen has less than 18", async function() {
        let cit1: Citizen = tree.get_leaf_from_id(0); // get cit0
        let merkle_proof = await tree.generate_proof(cit1);
        try {
            proof = await prover.prove(cit1,merkle_proof);
            assert(false);
        }
        catch (e) {
            assert(true);
        }
   });
});