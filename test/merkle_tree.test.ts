import assert from "assert";
import {MerkleProver} from "../src/merkle.prover";
import {MerkleVerifier} from "../src/merkle.verifier";
import {SnarkProof} from "../src/proof";
import {Citizen} from "../src/citizen";
import {MerkleTree} from "../src/merkle_tree";

describe("Test MerkleTree", function () {
    let prover: MerkleProver;
    let verifier: MerkleVerifier;
    let proof: SnarkProof;
    beforeEach(async () => {
        prover = new MerkleProver(2);
        verifier = new MerkleVerifier(2);
    });
    it("Should prove the merkle proof", async function() {
        const cit1 = new Citizen("Doe", "John", 1999);
        const cit2 = new Citizen("Doe2", "John2", 1999);
        const cit3 = new Citizen("Doe3", "John3", 1999);
        const cit4 = new Citizen("Doe4", "John4", 1999);
        let citizens = Array.from([cit1,cit2,cit3,cit4]);
        let tree = new MerkleTree(citizens);
        let root = await tree.root();
        let merkle_proof = await tree.generate_proof(cit4);

        proof = await prover.prove(cit4, merkle_proof);
        assert(proof.public_signals[1] === root);

        let v = await tree.verify_proof(merkle_proof, cit4);
        assert(v);

        let v2 = await tree.verify_proof(merkle_proof, cit1);
        assert(!v2);
    });
    it("Should verify the correct proof", async function() {

        let v = await verifier.verify(proof);
        assert(v);
    });
    it("Should detect the wrong proof", async function() {
       proof.public_signals[0] =  BigInt(1234);

        let v = await verifier.verify(proof);
        assert(!v);
    });
});