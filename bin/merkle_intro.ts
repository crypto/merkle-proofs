import {Citizen} from "../src/citizen";
import {MerkleTree} from "../src/merkle_tree";


async function main() {
    // create a merkle tree with 4 citizens
    const cit1 = new Citizen("Doe", "John", 1999);
    const cit2 = new Citizen("Doe2", "John2", 1999);
    const cit3 = new Citizen("Doe3", "John3", 1999);
    const cit4 = new Citizen("Doe4", "John4", 1999);
    let citizens = Array.from([cit1,cit2,cit3,cit4]);

    let tree = new MerkleTree(citizens);
    // get the root of the tree
    let root = await tree.root();

    // generate a proof for the citizen 4
    let cit4_merkle_proof = await tree.generate_proof(cit4);

    // verify the proof
    let verified = await tree.verify_proof(cit4_merkle_proof, cit4);
    console.log("Verified:", verified);

    // verify with a wrong citizen
    let verified2 = await tree.verify_proof(cit4_merkle_proof, cit1);
    console.log("Verified2:", verified2);
}

main().then(() => process.exit());