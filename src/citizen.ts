import Poseidon, {Hashable} from "./poseidon";

/**
 * Class representing a Citizen.
 * @implements {Hashable}
 */
export class Citizen implements Hashable {
    private _name: string;
    private _firstname: string;
    private _yearBirth: bigint;

    /**
     * Converts a string to a bigint by converting it to an array of bytes and then to a bigint.
     * @param {string} s - The string to be converted.
     * @returns {bigint} The bigint representation of the string.
     */
    private string_to_hex(s: string): bigint {
        const encoder = new TextEncoder();
        let c = encoder.encode(s);
        const hex = Array.from(c).map(b => b.toString(16).padStart(2, '0')).join('');
        return BigInt('0x' + hex);
    }

    /**
     * Creates a new Citizen object.
     * @param {string} _name - The name of the citizen.
     * @param {string} _firstname - The first name of the citizen.
     * @param {number} _yearBirth - The birth year of the citizen.
     */
    constructor(_name: string, _firstname: string, _yearBirth: number) {
        this._name = _name;
        this._firstname = _firstname;
        this._yearBirth = BigInt(_yearBirth);
    }

    /**
     * Gets the name of the citizen as a bigint.
     * @returns {bigint} The name of the citizen as a bigint.
     */
    public name(): bigint {
        return this.string_to_hex(this._name);
    }

    /**
     * Gets the first name of the citizen as a bigint.
     * @returns {bigint} The first name of the citizen as a bigint.
     */
    public firstname(): bigint {
        return this.string_to_hex(this._firstname);
    }

    /**
     * Gets the birth year of the citizen as a bigint.
     * @returns {bigint} The birth year of the citizen as a bigint.
     */
    public yearBirth(): bigint {
        return this._yearBirth;
    }

    /**
     * Hashes the citizen's information using the Poseidon hash function.
     * @returns {Promise<bigint>} A promise that resolves to the hash of the citizen's information.
     */
    async hash(): Promise<bigint> {
        const n = this.name();
        const f = this.firstname();
        const y = this.yearBirth();
        return await Poseidon.hash([n, f, y]);
    }
}