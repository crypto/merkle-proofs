import {Hashable} from "./poseidon";
import Poseidon from "./poseidon";

/**
 * Enum for the selectors of the Merkle tree.
 * LEFT and RIGHT are used to determine the position of a node in the tree.
 */
export enum Selector {
    LEFT,
    RIGHT
}

/**
 * Interface for the Merkle proof.
 * It contains the hashes, selectors and root of the Merkle tree.
 */
export interface MerkleProof {
    hashes: bigint[];
    selectors: Selector[];
    root: bigint;
}

/**
 * Class for the Merkle tree.
 * It contains methods for creating and manipulating a Merkle tree.
 */
export class MerkleTree<T extends Hashable> {
    /**
     * Constructor for the Merkle tree.
     * @param {T[]} leafs - The leaves of the tree.
     */
    constructor(private leafs: T[]) {}

    /**
     * Get a leaf from the tree by its ID.
     * @param {number} id - The ID of the leaf.
     * @returns {T} The leaf.
     */
    public get_leaf_from_id(id: number): T {
        return this.leafs[id];
    }

    /**
     * Compute the next level of the Merkle tree.
     * @param {bigint[]} hashes - The hashes of the current level.
     * @returns {Promise<bigint[]>} The hashes of the next level.
     */
    async compute_next_level(hashes: bigint[]){
        const hashes2 = [];
        for (let i = 0; i < hashes.length; i+=2) {
            hashes2.push(await Poseidon.hash([hashes[i],hashes[i+1]]));
        }
        return hashes2;
    }

    /**
     * Get the ID of a hash in the Merkle tree.
     * @param {BigInt[]} hashes - The hashes of the current level.
     * @param {BigInt} hash - The hash to find.
     * @returns {number} The ID of the hash.
     */
    get_id(hashes: BigInt[], hash: BigInt) {
        return hashes.findIndex(h => h.toString() === hash.toString());
    }

    /**
     * Compute the root of the Merkle tree.
     * @returns {Promise<BigInt>} The root of the tree.
     */
    async root() : Promise<BigInt> {
        let hashes = await Promise.all(this.leafs.map(cit => cit.hash()));
        while(hashes.length > 1) {
            hashes = await this.compute_next_level(hashes);
        }
        return hashes[0];
    }

    /**
     * Generate a proof for a leaf in the Merkle tree.
     * @param {T} leaf - The leaf to generate a proof for.
     * @returns {Promise<MerkleProof>} The proof for the leaf.
     */
    async generate_proof(leaf: T): Promise<MerkleProof> {
        const proof: bigint[] = [];
        const selectors: Selector[] = [];
        let id = this.leafs.findIndex(c => c === leaf);
        if (id == -1) throw new Error("Cannot find leaf into the Merkle tree");
        let hashes = await Promise.all(this.leafs.map(cit => cit.hash()));
        while(hashes.length > 1) {
            const h_leaf = hashes[id ^ 1];
            const h_concat = await Poseidon.hash([hashes[id & ~1], hashes[id | 1]]);
            proof.push(h_leaf);
            selectors.push(id % 2 == 0 ? Selector.LEFT : Selector.RIGHT);
            hashes = await this.compute_next_level(hashes);
            id = this.get_id(hashes, h_concat);
        }
        return { hashes: proof, selectors: selectors, root: hashes[0]};
    }

    /**
     * Verify a proof for a leaf in the Merkle tree.
     * @param {MerkleProof} proof - The proof to verify.
     * @param {T} leaf - The leaf to verify the proof for.
     * @returns {Promise<boolean>} True if the proof is valid, false otherwise.
     */
    async verify_proof(proof: MerkleProof, leaf: T): Promise<boolean> {
        let id = this.leafs.findIndex(c => c === leaf);
        if (id == -1) throw new Error("Cannot find leaf into the Merkle tree");
        let hashes = [await leaf.hash()];
        for (let i = 0; i < proof.hashes.length; i++) {
            const h_concat = await Poseidon.hash(proof.selectors[i] == Selector.LEFT ? [hashes[i], proof.hashes[i]] : [proof.hashes[i], hashes[i]]);
            hashes.push(h_concat);
        }
        return hashes[hashes.length-1].toString() === proof.root.toString();
    }
}