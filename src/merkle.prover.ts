import {SnarkProof} from "./proof";
import * as snarkjs from "snarkjs";
import {MerkleProof} from "./merkle_tree";
import {Hashable} from "./poseidon";
import fs from "fs";
import path from "path";

export class MerkleProver {
    private wasm: Buffer;
    private zKey: Buffer;


    constructor(levels: number) {
        const basePath = path.join(__dirname, `../zk`);
        this.wasm = fs.readFileSync(`${basePath}/circuits/merkle_verifier_${levels}_js/merkle_verifier_${levels}.wasm`);
        this.zKey = fs.readFileSync(`${basePath}/zkeys/merkle_verifier_${levels}_final.zkey`);
    }

    async prove(hashable_leaf: Hashable, merkle_proof: MerkleProof): Promise<SnarkProof> {
        let leaf = await hashable_leaf.hash();
        let root = merkle_proof.root;
        let hashes = merkle_proof.hashes;
        let selectors = merkle_proof.selectors;
        const { proof, publicSignals } = await snarkjs.groth16.fullProve(
            { leaf, root, hashes, selectors },
            this.wasm,
            this.zKey
        );
        return {
            proof:{
                a: [proof.pi_a[0], proof.pi_a[1]] as [bigint, bigint],
                b: [proof.pi_b[0].reverse(), proof.pi_b[1].reverse()] as [
                    [bigint, bigint],
                    [bigint, bigint]
                ],
                c: [proof.pi_c[0], proof.pi_c[1]] as [bigint, bigint],
            },public_signals:publicSignals};
    }
}