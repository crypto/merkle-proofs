import {SnarkProof, Groth16Verify} from "./proof";
import path from "path";

export class MerkleVerifier {
    private _verifierKey: string;

    constructor(k: number) {
        this._verifierKey = path.join(__dirname,`../zk/verifiers/merkle_verifier_${k}.vkey.json`);
    }

    async verify(proof: SnarkProof): Promise<boolean> {
        return Groth16Verify(this._verifierKey, proof);
    }
}