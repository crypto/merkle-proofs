import fs from "fs";
import path from "path";
import {SnarkProof} from "./proof";
import {Citizen} from "./citizen";
import {MerkleProof} from "./merkle_tree";
import * as snarkjs from "snarkjs";

export class IdCheckProver {
    private wasm: Buffer;
    private zKey: Buffer;

    constructor(levels: number) {
        const basePath = path.join(__dirname, `../zk`);
        this.wasm = fs.readFileSync(`${basePath}/circuits/id_check_${levels}_js/id_check_${levels}.wasm`);
        this.zKey = fs.readFileSync(`${basePath}/zkeys/id_check_${levels}_final.zkey`);
    }

    async prove(citizen: Citizen, merkle_proof: MerkleProof): Promise<SnarkProof> {
        let name = citizen.name();
        let firstname = citizen.firstname();
        let birthYear = citizen.yearBirth();
        let root = merkle_proof.root;
        let hashes = merkle_proof.hashes;
        let selectors = merkle_proof.selectors;
        let currentYear: bigint = BigInt(2024);
        const {proof, publicSignals} = await snarkjs.groth16.fullProve(
            {name, firstname, birthYear, root, hashes, selectors, currentYear},
            this.wasm,
            this.zKey
        );
        return {
            proof: {
                a: proof.pi_a as [bigint, bigint],
                b: [proof.pi_b[0].reverse(), proof.pi_b[1].reverse()] as [[bigint, bigint], [bigint, bigint]],
                c: proof.pi_c as [bigint, bigint],
            },
            public_signals: publicSignals
        };
    }
}