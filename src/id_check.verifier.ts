import {SnarkProof, Groth16Verify} from "./proof";
import path from "path";

export class IdCheckVerifier {
    private _verifierKey: string;

    constructor(levels: number) {
        this._verifierKey = path.join(__dirname,`../zk/verifiers/id_check_${levels}.vkey.json`);
    }

    async verify(proof: SnarkProof): Promise<boolean> {
        return Groth16Verify(this._verifierKey, proof);
    }
}