import * as ffjs from "ffjavascript";

/**
 * Converts a Buffer to an array of bigints.
 * @param {Buffer} buffer - The Buffer to be converted.
 * @param {number} chunkSize - The size of each chunk to be converted to a bigint.
 * @returns {bigint[]} An array of bigints.
 */
export function bufferToBigIntArray(buffer: Buffer, chunkSize: number): bigint[] {
    const bigIntegers: bigint[] = [];
    for (let i = 0; i < buffer.length; i += chunkSize) {
        const chunk = buffer.subarray(i, i + chunkSize);
        if (chunk.length < chunkSize) {
            const paddedChunk = Buffer.alloc(chunkSize);
            chunk.copy(paddedChunk);
            bigIntegers.push(BigInt('0x' + paddedChunk.toString('hex'))); // Convert chunk to BigInt
            continue;
        }
        const bigIntValue = BigInt('0x' + chunk.toString('hex')); // Convert chunk to BigInt
        bigIntegers.push(bigIntValue);
    }
    return bigIntegers;
}

/**
 * Converts an array of bigints to a Buffer.
 * @param {bigint[]} bigIntegers - The array of bigints to be converted.
 * @param {number} chunkSize - The size of each chunk in the resulting Buffer.
 * @returns {Promise<Buffer>} A promise that resolves to a Buffer.
 */
export async function bigIntArrayToBuffer(bigIntegers: bigint[], chunkSize: number): Promise<Buffer> {
    let curve = await ffjs.getCurveFromName('bn128',true);
    let F = curve.Fr;
    let bytes =[];
    for (const bigInt of bigIntegers) {
        let a=F.toString(bigInt, 16);
        let hexString = a.length % 2 === 0 ? a : '0' + a;
        while (hexString.length < chunkSize * 2) {
            hexString = '0' + hexString;
        }
        for (let i = 0; i < hexString.length; i += 2) {
            const byte = parseInt(hexString.slice(i, i + 2), 16);
            bytes.push(byte);
        }
    }
    return Buffer.from(bytes);
}

/**
 * Converts a bigint to a Buffer.
 * @param {BigInt} bigInt - The bigint to be converted.
 * @returns {Promise<Buffer>} A promise that resolves to a Buffer.
 */
export async function bigintToBuffer(bigInt: BigInt): Promise<Buffer> {
    let curve = await ffjs.getCurveFromName('bn128',true);
    let F = curve.Fr;
    let a=F.toString(bigInt, 16);
    let hexString = a.length % 2 === 0 ? a : '0' + a;
    while (hexString.length < 64) {
        hexString = '0' + hexString;
    }
    return Buffer.from(hexString, 'hex');
}